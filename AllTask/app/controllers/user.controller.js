//import thư viện mongoose
const mongoose = require('mongoose');

// import User model
const userModel = require('../models/user.model');

// const create User 
const createUser = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        fullName,
        phone,
        status,
    } = req.body;

    // B2: Validate dữ liệu
    
    if (!fullName) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "fullName is require"
        })
    }

    if (!phone) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "phone is require"
        })
    }

    // B3: Thao tác với CSDL
    var newUser = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        phone,
        status,
    }

    try {
        const createdUser = await userModel.create(newUser);
        return res.status(201).json({
            status: "Create User successfully",
            data: createdUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllUser = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
            const userList = await userModel.find();

            if(userList && userList.length > 0) {
                return res.status(200).json({
                    status: "Get all User sucessfully",
                    data: userList                })
            } else {
                return res.status(404).json({
                    status: "Not found any User"
                })
            }
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const userInfo = await userModel.findById(userId);

        if (userInfo) {
            return res.status(200).json({
                status:"Get User by id sucessfully",
                data: userInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any User"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateUser = async (req, res) => {
    //B1: thu thập dữ liệu
    var userId = req.params.userId;

    const {fullName,
        phone,
        status,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "fullName is require"
        })
    }

    if (!phone) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "phone is require"
        })
    }

    //B3: thực thi model
    try {
        let updateUser = {
            fullName,
            phone,
            status,
        }

        const updatedUser = await userModel.findByIdAndUpdate(userId, updateUser);

        if (updatedUser) {
            return res.status(200).json({
                status:"Update User sucessfully",
                data: updatedUser
            })
        } else {
            return res.status(404).json({
                status:"Not found any User"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteUser = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);
    
        if (deletedUser) {
            return res.status(200).json({
                status:"Delete User sucessfully",
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status:"Not found any User"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllUser,
    createUser,
    getUserById,
    updateUser,
    deleteUser
}

