const express = require("express");

const contactMiddleware = require("../middlewares/contact.middlewares");
const contactController = require("../controllers/contact.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL contact : ", req.url);

    next();
});

router.get("/", contactMiddleware.getAllContactMiddleware, contactController.getAllContact)

router.post("/", contactMiddleware.createContactMiddleware, contactController.createContact);

router.get("/:contactId", contactMiddleware.getDetailContactMiddleware, contactController.getContactById);

router.put("/:contactId", contactMiddleware.updateContactMiddleware, contactController.updateContact);

router.delete("/:contactId", contactMiddleware.deleteContactMiddleware, contactController.deleteContact);


module.exports = router;
