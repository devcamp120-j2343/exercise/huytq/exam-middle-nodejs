//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        validate: {
          validator: function(v) {
            return /(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})\b/.test(v);
          },
          message: props => `${props.value} is not a valid phone number!`
        },
        required: [true, 'User phone number required']
      },
    status: {
        type: String,
        default: "Level 0"
    },
},{timestamps: true});

//b4 biên dịch schema thành model
module.exports = mongoose.model("user",userSchema)
