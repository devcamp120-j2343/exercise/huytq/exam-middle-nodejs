//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')
//khai bao thu vien unique validator
const uniqueValidator = require('mongoose-unique-validator');
//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const contactSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    email: {
        type: String,
        lowercase: true,
        required: [true, "can't be blank"],
        unique: true,
        match: [/\S+@\S+\.\S+/, 'is invalid'],
        index: true
    },
    
},{timestamps: true});
contactSchema.plugin(uniqueValidator, {message:'is already taken.'});
//b4 biên dịch schema thành model
module.exports = mongoose.model("contact",contactSchema)
