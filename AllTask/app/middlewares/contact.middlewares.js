const getAllContactMiddleware = (req, res, next) => {
    console.log("Get all Contact  Middleware");

    next();
}

const createContactMiddleware = (req, res, next) => {
    console.log("Create Contact  Middleware");

    next();
}

const getDetailContactMiddleware = (req, res, next) => {
    console.log("Get detail Contact  Middleware");

    next();
}

const updateContactMiddleware = (req, res, next) => {
    console.log("Update Contact  Middleware");

    next();
}

const deleteContactMiddleware = (req, res, next) => {
    console.log("Delete Contact  Middleware");

    next();
}

module.exports = {
    getAllContactMiddleware,
    createContactMiddleware,
    getDetailContactMiddleware,
    updateContactMiddleware,
    deleteContactMiddleware
}

