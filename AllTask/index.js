//Import thư viện ExpressJS
// import express from 'express';
const express = require('express');
const cors = require('cors');
const path = require('path');
//import thư viện mongoose
const mongoose = require('mongoose');

// Khởi tạo app nodejs bằng express
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

//khai bao router
const userRouter = require("./app/routes/user.router");
const contactRouter = require("./app/routes/contact.router");

// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());
app.use(cors());

//Để hiển thị ảnh cần thêm middleware static vào express
app.use(express.static(__dirname + "/app/views/page"));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/page/index.html"));
})
app.get("/admin/user",(request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/app/views/admin/user/userManagement.html"));
  })
app.get("/admin/contact",(request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/app/views/admin/contact/contactManagement.html"));
  })
//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
    var today = new Date();

    console.log("Current time: ", today);

    next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((request, response, next) => {
    console.log("Method: ", request.method);

    next();
});


//ket noi database mongo
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vaccination")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));

//khai báo các model
const userModel = require('./app/models/user.model');
const contactModel = require('./app/models/contact.model');
//khai bao api 
app.use("/api/v1/user", userRouter);
app.use("/api/v1/contact",contactRouter);

// Chạy ứng dụng
app.listen(port, () => {
    console.log("Ứng dụng chạy trên cổng: ", port);
})